terraform {
  backend "s3" {
    bucket = "micro-app-2023"
    region = "eu-west-2"
    key = "eks/terraform.tfstate"
  }
}
